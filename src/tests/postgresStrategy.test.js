const assert = require('assert')
const Postgres = require('./../db/strategies/postgres')
const Context = require('./../db/strategies/base/contextStrategy')

const context = new Context(new Postgres())

const MOCK_HEHOI_CADASTRAR = {
    nome: 'Batmano',
    poder: 'money'
}

const MOCK_HEHOI_ATUALIZAR = {
    nome: 'Aquaman',
    poder: 'falar golfinho'
}

describe('Postgres Strategy', function () {
    this.timeout(Infinity);
    this.beforeAll(async function () {
        await context.connect()
        await context.delete()
        await context.create(MOCK_HEHOI_ATUALIZAR)
    })
    it('PostgresDB Connection', async function () {
        const result = await context.isConnected()
        assert.equal(result, true)
    })
    it('cadastrar', async function () {
        const result = await context.create(MOCK_HEHOI_CADASTRAR)
        delete result.id
        assert.deepEqual(result, MOCK_HEHOI_CADASTRAR)
    })
    it('listar', async function () {
        const [result] = await context.read({ nome: MOCK_HEHOI_CADASTRAR.nome })
        delete result.id

        assert.deepEqual(result, MOCK_HEHOI_CADASTRAR)
    })
    it('atualizar', async function () {
        const [itemUpdate] = await context.read({ nome: MOCK_HEHOI_ATUALIZAR.nome })
        const newItem = {
            ...MOCK_HEHOI_ATUALIZAR,
            nome: 'Thor'
        }
        const [result] = await context.update(itemUpdate.id, newItem)
        assert.deepEqual(result, 1)
    })
    it('remover', async function () {
        const [item] = await context.read({})
        const result = await context.delete(item.id)
        assert.deepEqual(result, 1)
    })
})