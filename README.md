docker run \
    --name postgressdb \
    -e POSTGRES_USER=admin \
    -e POSTGRES_PASSWORD=admin \
    -e POSTGRES_DB=heroes \
    -p 5432:5432 \
    -d \
    postgresdb

Entrar no bash do Postgres
docker exec -it postgres /bin/bash

sudo docker pull adminer
sudo docker run \
    --name adminer \
    -p 8090:8080 \
    --link postgresdb:postgresdb \
    -d \
    adminer

##----------------- MONGODB
docker run \
    --name mongodb \
    -p 27017:27017 \
    -e MONGO_INITDB_ROOT_USERNAME=admin \
    -e MONGO_INITDB_ROOT_PASSWORD=admin \
    -d \
    mongo


docker run \
    --name mongoclient \
    -p 3020:3000 \
    --link mongodb:mongodb \
    -d \
    mongoclient/mongoclient

docker exec -it mongodb \
    mongo --host localhost -u admin -p admin --authenticationDatabase admin \
    --eval "db.getSiblingDB('herois').createUser({user: 'rafera', pwd: 'admin', roles: [{role: 'readWrite', db: 'herois'}]})"